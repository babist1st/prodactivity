from gi.repository import Gtk, Gio

from .gui.handlers import Handler
from .gui.perspectives import Perspective
from .gui.properties import Properties
from .data.sql import database
from .data.sql import database

@Gtk.Template(resource_path='/org/gnome/prodactivity/assets/gui/prodactivity.ui')
class Prodactivity(Gtk.ApplicationWindow):
    __gtype_name__ = 'Prodactivity'
    new_task_button = Gtk.Template.Child()
    delete_task_button = Gtk.Template.Child()

    inbox_scroll = Gtk.Template.Child()

    main_box = Gtk.Template.Child()
    main_stack = Gtk.Template.Child()
    def __init__(self,**kwargs):
        print('initializing interface')
        super().__init__(**kwargs)

    def startup(self, *args, **kwargs):
        print('starting up')
        #properties=self.builder.get_object("mainProperties")
        #self.myData = database('test.db')
        #TODO: create database if it doesn't exist
        self.myData = database('/home/babist1st/.cache/gnome-builder/install/prodactivity/host/share/prodactivity/prodactivity/test.db')
        #self.myData = database('/org/gnome/prodactivity/test.db')
        #self.myData = database('/home/babist1st/Projects/prodactivity/test.db')


        self.activeperspective = Perspective(self.myData,self)
        self.activeperspective.populate_store()
        self.activeperspective.view.expand_all()

        self.properties = Properties(self)

        self.activeperspective.InboxTreeView.connect("row-activated", self.on_view_row_activated)
        self.activeperspective.myTreeStore.connect("row-changed", self.on_store_row_changed)
        self.activeperspective.inboxSelection.connect("changed", self.on_TreeSelection_changed)
        #self.revealer = self.propertiesRevealer
        print('Attaching Properties')
        self.main_box.pack_end(self.properties.propertiesRevealer, False, False,0)
        print(self.properties.propertiesRevealer.get_reveal_child())
        #self.properties.propertiesRevealer.set_reveal_child(False)



        #self.revealer.attach(self.properties.mainProperties)

        #self.properties.update()

        #self.builder.connect_signals(Handler(self))

        #self.inbox_scroll.add_with_viewport(self.activeperspective.InboxTreeView)
        self.inbox_scroll.add(self.activeperspective.InboxTreeView)
        #self.inbox = Perspective(self.myData, self.builder)
                                #modelStore = self.myTreeStore,
                                #view = self.InboxTreeView)
        #self.inbox.populate_store()
        #self.inbox.view.expand_all()

        #self.projects = Perspective(self.myData, self.builder,
        #                        modelStore = "myTreeStore",
        #                        view = "ProjectsTreeView")
        #self.projects.view.expand_all()
        #self.contexts = Perspective(self.myData, self.builder,
        #                        modelStore = "myTreeStore",
        #                        view = "ContextsTreeView")
        #self.contexts.view.expand_all()
        #self.calendar = Perspective(self.myData, self.builder,
        #                        modelStore = "myTreeStore",
        #                        view = "CalendarTreeView")
        #self.calendar.view.expand_all()

        self.new_task_button.connect("clicked", self.activeperspective.add_item)
        self.delete_task_button.connect("clicked",
                            self.activeperspective.delete_item,
                            self.main_stack)

        #self.filtertask = self.builder.get_object("test_button")
        #self.filtertask.connect ("clicked",
        #                    self.filter_button)


    def filter_view(self):
        self.create_filter(self.inbox.store)
        self.inbox.view.set_model(self.fvar)
        print('shit')
        self.inbox.view.expand_all()

    def create_filter(self, store):
        self.fvar = store.filter_new()
        self.fvar.set_visible_func(self.activeperspective.filter_item)
        # self.view.set_model(self.filter)

    def filter_button(self,*args):
        treeiter = self.activeperspective.get_selected_row()
        self.activeperspective.stat = self.activeperspective.store[treeiter][6]
        print(self.activeperspective.stat)
        try:
            self.filter_view()
        except:
            print(e)
        print(self.activeperspective.stat)
        # self.stat =  0
        self.fvar.refilter()

    def passme(self, *args):
        print (*args)
        print('pass')

    #@Gtk.Template.Callback()
    #def on_main_delete_event(self,*args):
        #Gtk.main_quit()

    def on_view_row_activated(self, *args):
        if self.properties.propertiesRevealer.get_reveal_child():
            self.properties.propertiesRevealer.set_reveal_child(False)
        else:
            self.properties.propertiesRevealer.set_reveal_child(True)

    def on_store_row_changed(self, store, treepath, treeiter):
        #TODO: Figure out how to use only the first show_on_selection_changed
        item = store.get_value(treeiter,0)
        print ('item moved', item)
        s=treepath.up()
        if treepath.get_depth() > 0:
            parent = store.get_value(store.get_iter(treepath),0)
            print('into item', parent)
        else:
            parent = None
        self.activeperspective.data.update("parent", parent, item)
        #self.appInstance.properties.update()

    def on_TreeSelection_changed(self, selection):
        self.properties.update()
