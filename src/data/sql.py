#! /usr/bin/python

#import sqlite3 as lite
import sqlite3 
import sys

#con = None

class database():
    def __init__(self,db):
        self.con = sqlite3.connect(db)
        self.con.row_factory=sqlite3.Row #dictionary

        self.cur = self.con.cursor()
        with self.con:
            self.read()

    def create(self):
        #self.cur.execute("CREATE TABLE newtable (Id INTEGER PRIMARY KEY, Title TEXT, Description TEXT, Type INTEGER)")
        pass

    def add(self,l):
        self.cur.execute("INSERT INTO items(Title, Description) VALUES(?,?)",
                         (l[1],l[2]))
        return True

    def delete(self,id):
        self.cur.execute("DELETE FROM items WHERE Id=?",(id,))

    def write(self):
        pass

    def update(self,column,value,id):
        #self.cur.execute("UPDATE items Set Name=? WHERE Id=?",('bbb','3'))
        #print(column,value,id)
        self.cur.execute("UPDATE items SET '{}' = ? WHERE id=?".format(column),(value,id))

    def get_id(self, id):
        return self.cur.execute("select * from items where id = ? ",(id,)).fetchone()

    def get_maxid(self):
        return self.cur.execute("SELECT MAX(id) AS max_id FROM items").fetchone()

    def read_parents (self):
        return self.cur.execute("SELECT * FROM items where parent is NULL ").fetchall()

    def read_children (self, parent):
        return self.cur.execute("SELECT * FROM items where parent = ? ",(parent,)).fetchall()
        #self.children_rows= self.cur.fetchall()

    def read(self):
        self.cur.execute("SELECT * FROM items")
        self.rows= self.cur.fetchall()
        #print (self.rows)
        print("Just read a table")
        print("Dude it was awesome")
        pass


    def print(self):
        print (self.rows)
        #print(self.rows[0].keys())
        #print(tuple(self.rows[0].keys()))
        for row in self.rows:
            print(tuple(row))
            #print(row)
            #print(row[0])

    def close(self):
        self.con.close()


#data=database('test.db')
#print(data.con)
##data.close()
#with data.con:
#    #data.update()
#    data.read()
#    data.print()

#con = lite.connect('test.db')
#with con:
#    #con.execute("CREATE TABLE Items(Id INT PRIMARY KEY, Name TEXT, Description TEXT, Type INT)")
#    #con.execute("INSERT INTO Items VALUES('1','fit','A life time gone by',0)")
#    #con.execute("INSERT INTO Items VALUES('2','London Trop','Fun is fun',1)")
#    #con.execute("INSERT INTO Items VALUES('3','Bike','Exercise is fun',1)")
#    #con.execute("INSERT INTO Items VALUES('4','Computer','Bain of my life',1)")
#    #con.execute("INSERT INTO Items VALUES('5','ICBAs','oh come on',0)")
#
#    con.row_factory=lite.Row #dictionary
#    cur = con.cursor()
#    cur.execute("UPDATE Items Set Name=? WHERE Id=?",('upBike','3'))
#    cur.execute("SELECT * FROM Items")
#    #cur=con.execute("SELECT * FROM Items")
#    rows= cur.fetchall()
#    print (rows)
#    for row in rows:
#        print (row['Id'])
#        print (tuple(row))
#        print (row.keys())
#        for k in row.keys():
#            print (row[k])
#            print (row[1])
#        #print (row.items())
#
#
#
#
#def version():
#    with con:
#        con = lite.connect('test.db')
#           
#        cur = con.cursor()
#        cur.execute('SELECT SQLITE_VERSION()')
#    
#        data = cur.fetchone()
#    
#        print ("SQLite version: %s" %data)
#    
