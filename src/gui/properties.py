#! /usr/bin/python

from gi.repository import Gtk
from gi.repository import GObject

@Gtk.Template(resource_path='/org/gnome/prodactivity/assets/gui/properties.ui')
class Properties(Gtk.Revealer):
    __gtype_name__ = 'propertiesRevealer'
    mainViewport = Gtk.Template.Child()
    mainProperties = Gtk.Template.Child()
    propertiesRevealer = Gtk.Template.Child()

    Title = Gtk.Template.Child()
    Description = Gtk.Template.Child()
    date_modified = Gtk.Template.Child()
    date_added = Gtk.Template.Child()

    layout = Gtk.Template.Child()

    statusComboBox = Gtk.Template.Child()
    statuses = Gtk.Template.Child()
    def __init__(self, appInstance, **kwargs):
        super().__init__(**kwargs)
        self.appInstance = appInstance
        self.perspective = appInstance.activeperspective

        #self.revealer = self.builder.get_object("propertiesRevealer")
        #self.revealer = self.propertiesRevealer
        self.fields = {
            "Title": self.Title,
            "Description": self.Description,
            "Date Added": self.date_added,
            "Date Modified": self.date_modified
        }

        #expander = self.builder.get_object("TitleExpander")
        #entry = self.builder.get_object("Title")
        #expander.connect("notify::expanded",self.expander_activate2,entry)
        #self.expander_activate2(expander,'True',entry)

        #if expander.get_expanded():
            #expander.set_expanded('False')
            #expander.set_expanded('True')
        self.status()

        #if self.revealer.get_reveal_child():
          #  pass

    #def expander_activate2(self, expander, state, entry):
    #    if expander.get_expanded():
    #        entry.grab_focus()
    #        print("poutsa kai xilo")
    #    else:
    #        print("giati?")

    #def expander_activate(self,*args):
    #    self.rev = self.builder.get_object("rev")
    #    expander = self.builder.get_object("TitleExpander")
    #    print(expander.get_expanded())
    #    expander.connect("activate",self.expander_activate)
    #    if self.rev.get_reveal_child():
    #        self.rev.set_reveal_child(False)
    #    else:
    #        self.rev.set_reveal_child(True)
    def status(self):
        #TODO: This should be put somewhere that i will collect all the data at the loading of the db
        statuses2 = self.perspective.data.cur.execute("select * from status").fetchall()
        self.perspective.data.cur.execute("select * from items")

        self.status_combo_box = self.statusComboBox
        #self.status_combo_box = self.builder.get_object("statusComboBox")
        #statuses_list_store = self.builder.get_object("statuses")
        statuses_list_store = self.statuses
        self.status_combo_box.set_entry_text_column(1)

        len(statuses2)
        for status in statuses2:
            #print(list(status))
            statuses_list_store.append(list(status))

        self.status_combo_box.connect("changed", self.on_name_combo_changed)
        renderer_text = Gtk.CellRendererText()
        self.status_combo_box.pack_start(renderer_text, True)
        self.status_combo_box.add_attribute(renderer_text, "text", 1)

    def on_name_combo_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            row_id, name = model[tree_iter][:2]
            print("Selected: ID=%d, name=%s" % (row_id, name))
        else:
            entry = combo.get_child()
            print("Entered: %s" % entry.get_text())

    def mine(self,parent):
        try:
            for t in parent.get_children():
                if isinstance(t,Gtk.Entry) or isinstance(t,Gtk.TextView):
                    self.props.append(t)
                else:
                    self.mine(t)
        except (AttributeError):
            print ('No children',parent)


    #TODO: Perhaps move it to Prodactivity or Perspective
    def update(self):
        #if self.propertiesRevealer.get_reveal_child():
        #if self.appInstance.activeperspective.selection:
        stack=self.appInstance.main_stack
        child_name = stack.get_visible_child_name()
        self.perspective.view = self.appInstance.activeperspective.view
        #self.perspective.view = self.builder.get_object(
        #                                    child_name+"TreeView")
        #for index in range(self.perspective.store.get_n_columns()-1):
        #    clmn= self.perspective.view.get_column(index)
        #    print (index,clmn.get_title())

        #for i in ["Title","Description"]:
        for column in self.perspective.view.get_columns():
            i = column.get_sort_column_id()
            b = column.get_title()
            #entry = self.Title #self.builder.get_object(column.get_title())
            #print(i,b,entry)

            #print(self.layout.get_children())
            #self.mine(self.layout)

            try:
                treeiter = self.perspective.get_selected_row()
                txt = self.perspective.store.get_value(treeiter,i)
                #BUG: None fields(Date Modified) throw errors
                if txt == None:
                    txt = ""

                if b == "Description":
                    entrybuffer = Gtk.TextBuffer.new()
                    entrybuffer.set_text(txt,len(txt))
                elif i == 6:
                    self.status_combo_box.set_active(txt)
                    return
                else:
                    entrybuffer = Gtk.EntryBuffer.new(txt,len(txt))

                entry = self.fields[b]#self.builder.get_object(column.get_title())
                entry.set_buffer(entrybuffer)

                #print(entry.connect("changed",self.editing, treeiter))
            #except (AttributeError):
            #    print('ti mou zitas re file')
            except (TypeError) as e:
                print ('Wrong type given for properties field', e)
            except (UnboundLocalError):
                print('Reloaded model? reseletct seomthing')


        #i should check if it is revealed
        #if it is i should display the options
        #for the item selected in this view

        #a function to update the item on click
        #would be usefull

        #options include:
        #name/description
        #dates
        #tags
        #others as they come up.

    def editing(self, entry, treeiter):
        txt = entry.get_buffer().get_text()
        self.appInstance.perspective.store.set_value(treeiter,1,txt)
 
