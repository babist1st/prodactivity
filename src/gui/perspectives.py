#! /usr/bin/python

from gi.repository import Gtk
from gi.repository import GObject
import datetime

@Gtk.Template(resource_path='/org/gnome/prodactivity/assets/gui/perspective.ui')
class Perspective(Gtk.TreeView):
    __gtype_name__ = 'Perspective'
    InboxTreeView = Gtk.Template.Child()
    myTreeStore = Gtk.Template.Child()

    inboxSelection = Gtk.Template.Child()
    def __init__(self, myData, appInstance, modelStore = None, view = None, **kwargs):
        super().__init__(**kwargs)
        self.appInstance = appInstance
        self.data = myData
        self.selection = self.inboxSelection
        self.view = view
        self.stat= None

        self.store = self.myTreeStore
        if modelStore != None:
            #self.store = self.builder.get_object(modelStore)
            self.store = modelStore

        self.view = self.InboxTreeView
        if view != None:
            pass
            #self.view = self.builder.get_object(view)
            #self.view = view
            #print(self.store)
            #self.selection = self.view.get_selection()
        self.initialize_view()

    def initialize_view(self):
        states = Gtk.ListStore(int,str)
        statuses = self.data.cur.execute("select * from status").fetchall()
        self.data.cur.execute("select * from items")
        for state in statuses:
            states.append(list(state))

        self.categories = tuple(map(lambda x: x[0], self.data.cur.description))
        for i, column_title in enumerate(self.categories):
            renderer=Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title,renderer,text=i)
            if i == 3 or i == 4:
                renderer.set_property("editable", False)
            elif i == 0 or i == 5:
                renderer.set_property("editable", False)
                column.set_visible(False)
                continue
            elif i == 6:
                renderer = Gtk.CellRendererCombo()
                renderer.set_property("editable", True)
                renderer.set_property("model",states)
                renderer.set_property("text-column",1)
                renderer.set_property("has-entry",False)
                renderer.connect("edited",self.update_combo,i,column_title)
                column = Gtk.TreeViewColumn(column_title,renderer,text=i)#, active=True)
                self.view.append_column(column)
                column.set_sort_column_id(i)
                continue
            #elif i == 6:
            #    renderer = Gtk.CellRendererToggle()
            #    column = Gtk.TreeViewColumn(column_title,renderer)#, active=True)
            #    renderer.connect("toggled",self.passme)
            #    self.view.append_column(column)
            #    continue
            else:
                renderer.set_property("editable", True)
                renderer.set_property("placeholder_text", "Empty")
            column.set_resizable(True)
            column.set_max_width(150)
            column.set_min_width(80)
            renderer.connect("edited", self.update_edit, i, column_title)
            #print (column)
            #GObject.type_set_qdata(renderer,1,column)

            self.view.append_column(column)
            column.set_sort_column_id(i)
        #self.view.append_column(column)

    def passme(self,*args):
        print("hello",args)

    def populate_view(self):
        for item_ref in self.data.rows:
            print (list(item_ref))
            self.show_item(None, item_ref)

    def delete_item(self, *args):
        #print (*args)
        for i, thing in enumerate(args):
            stack = thing
            #print (stack)

        #child_name = stack.get_visible_child_name()
        #self.view = self.builder.get_object(child_name+"TreeView")

        print(stack.get_visible_child().get_children())
        self.view = stack.get_visible_child().get_children()[0]

        self.selection = self.view.get_selection()
        #print(self.view.get_model())
        #print(self.selection)

        try:
            treeiter = self.get_selected_row()
            id=self.store.get_value(treeiter,0)
        except UnboundLocalError:
            print ('Nothing selected to delete')
            return

        self.data.delete(id)
        self.hide_item(treeiter)

    def get_selected_row(self):
        try:
            model, treepathlist = self.selection.get_selected_rows()
            self.store = model
            #print (model)
            #print(model,treepathlist)
            for p in range(len(treepathlist)):
                treeiter = model.get_iter(treepathlist[p])
        except AttributeError:
            print ('Nothing selected Attribute')
            return
        except UnboundLocalError:
            print ('Nothing selected Unbound')
            return

        return treeiter

    def add_item(self, *args):
        #myStore.append(None,list([77,"shit","shit",0]))
        #e=self.db.data.cur.lastrowid
        e = self.data.get_maxid()
        if list(e)[0] == None :
            maxid = 1
        else:
            maxid = list(e)[0]+1
        #TODO: Bring up a menu to populate the task fields
        l=["","","","","",""]
        self.data.add(l)

        l=self.data.get_id(maxid)
        parent_treeiter = None
        #An atempt to get as parent the selected row
        #self.selection = self.view.get_selection()
        #model, treepathlist = self.selection.get_selected_rows()
        ##print(treepathlist)
        #for p in range(len(treepathlist)):
        #    parent_treeiter = model.get_iter(treepathlist[p])
        #    print(treeiter)
        self.show_item(parent_treeiter, l)

    def update_combo(self, cellRenderer, path, new_text,col,column_title):
        stateslist=list(cellRenderer.props.model)
        m = self.store
        row = m[path]
        old_text = self.store.get_value(row.iter,col)
        print(stateslist[0][1])
        print(m,list(row),new_text,old_text)
        if new_text != old_text :
            rowl = list(m[path])
            rowl[col] = new_text

            id = rowl[0]
            for i in range(len(stateslist)):
                print (i)
                if new_text == stateslist[i][1]:
                    self.store.set_value(row.iter, col, stateslist[i][0])
                    #self.store.set_value(row.iter, col, "b")
                    self.data.update(column_title, i, id)

    def update_edit(self, cellRenderer, path, new_text, col, column_title):
        #Date should update only on new text
        m = self.store
        row = m[path]
        old_text = self.store.get_value(row.iter,col)
        if new_text != old_text :

            rowl = list(m[path])

            rowl[col] = new_text
            if col == 6:
                #self.store.set_value(row.iter,col,1)
                print()
                self.store[path][1]= new_text
            else:
                self.store.set_value(row.iter,col,new_text)

        #id=self.store.get_value(row.iter,0)
            id = rowl[0]
            if col == 6:
                self.data.update(column_title, 1, id)
            else:
                self.data.update(column_title, new_text, id)

            now = datetime.datetime.now()
            self.store.set_value(row.iter, 4, str(now))
            self.data.update("Date Modified", now, id)

        #print(path,col,new_text)

    def show_item(self, parent, item_ref):
        return self.store.append(parent,list(item_ref))

    def populate_store(self):
        parents = self.data.read_parents()
        for item_ref in parents:
            treeiter = self.show_item(None, item_ref)
            parent = list(item_ref)[0]
            #self.data.read_children(parent))
            self.populate_children(parent,treeiter)

    def populate_children(self, parent, parent_treeiter):
        children = self.data.read_children(parent)
        for item_ref in children:
            treeiter = self.show_item(parent_treeiter, item_ref)
            pp = list(item_ref)[0]
            self.populate_children(pp, treeiter)


    def hide_item(self, treeiter):
        self.store.remove(treeiter)

    def filter_item(self, model, iter, data):
        if self.stat is None:
            return True
        else:
            return model[iter][6] == self.stat
