#! /usr/bin/python

import sys
import signal

import gi
gi.require_version('Gtk','3.0')
from gi.repository import  Gtk, Gio

from .window import Prodactivity

class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='org.gnome.prodactivity',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self, *args, **kwargs):
        print('activating')

        #self.window = self.builder.get_object("main")
        self.window = self.props.active_window
        if not self.window:
            self.window = Prodactivity()
        #window.show_all()
        self.window.present()
        self.window.startup()
        #self.window.present()
        #self.window.show()
        #self.window.show_all()
        Gtk.main()

    #def do_open(self):
    #    pass

    def shutdown(self, *args):
        print(self.myData.con.total_changes)
        self.myData.con.commit()
        print('shutting down')

def main(version):
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = Application()
    app.run(sys.argv)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = Application()
    #app.run(sys.argv)
